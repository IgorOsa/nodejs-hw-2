# RD LAB NODEJS HOMEWORK №2

Use NodeJS to implement CRUD API for notes.

## Requirements

- [x] Mandatory npm start script.
- [x] Please check that your app can be launched with 'npm install' and 'npm start' commands just after git pull;
- [x] Ability to run server on port which is defined as PORT environment variable, default to 8080;
- [x] Use express to implement web-server;
- [x] Use express Router for scaling your app and MVC pattern to organize project structure;
- [x] Follow REST API rules described in Swagger file;
- [x] Encode all users password with BCRYPT npm package
- [x] Use config or dotenv npm packages to store configuration for your project;
- [x] Use jsonwebtoken package for jwt authorization;
- [x] Use Mongo Atlas for MongoDB connection;
- [x] Server handles errors for all requests;
- [x] Write every request info to logs;
- [x] Application code should follow eslint rules described in .eslintrc.json file(requires eslint and eslint-config-google packages install);

## Acceptance criteria

- [x] Ability to register users;
- [x] User is able to login into the system;
- [x] User is able to view his profile info;
- [x] User is able to view only personal notes, provide pagination parameters for notes list, request note by id;
- [x] User is able to add, delete personal notes;
- [x] User can check/uncheck any note;
- [x] User can manage notes and personal profile only with valid JWT token in request;

## Optional criteria

- [x] User can change his profile password;
- [x] User can delete personal account;
- [x] Ability to edit personal notes text;
- [ ] Simple UI for your application(would be a big plus).
