import mongoose from 'mongoose';

const userSchema = new mongoose.Schema(
    {
      username: {
        type: String,
        unique: true,
        required: true
      },
      createdDate: {
        type: Date,
        default: Date.now()
      }
    },
    {
      collection: 'users',
      versionKey: false
    }
);

const User = mongoose.model('User', userSchema);

export default User;
