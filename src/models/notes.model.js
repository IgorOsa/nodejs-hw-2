import mongoose from 'mongoose';

const notesSchema = new mongoose.Schema(
    {
      userId: {
        type: String,
        required: true
      },
      completed: {
        type: Boolean,
        default: false
      },
      text: String,
      createdDate: Date
    },
    {
      collection: 'notes',
      versionKey: false
    }
);

// eslint-disable-next-line require-jsdoc
async function setCreationDate(next) {
  // eslint-disable-next-line no-invalid-this
  this.createdDate = Date.now();
  next();
}

notesSchema.pre('save', setCreationDate);

const Note = mongoose.model('Note', notesSchema);

export default Note;
