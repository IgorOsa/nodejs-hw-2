/* eslint-disable no-invalid-this */
/* eslint-disable require-jsdoc */
import mongoose from 'mongoose';
import {hashPassword} from '../auth/hashHelpers';

const credentialsSchema = new mongoose.Schema(
    {
      username: {
        type: String,
        unique: true,
        required: true
      },
      password: {
        type: String,
        required: true
      }
    },
    {
      collection: 'credentials',
      versionKey: false
    }
);

async function setHashedPassword(next) {
  this.password = await hashPassword(this.password);
  next();
}

async function preUpdate(next) {
  if (this._update.$set.password) {
    this._update.$set.password = await hashPassword(this._update.$set.password);
  }
  next();
}

credentialsSchema.pre('save', setHashedPassword);
credentialsSchema.pre('findOneAndUpdate', preUpdate);

const Credentials = mongoose.model('Credentials', credentialsSchema);

export default Credentials;
