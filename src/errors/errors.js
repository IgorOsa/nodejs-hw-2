import {StatusCodes, ReasonPhrases} from 'http-status-codes';

/* eslint-disable require-jsdoc */
export class BadRequestError extends Error {
  constructor(message) {
    super(message || ReasonPhrases.BAD_REQUEST);
    this.status = StatusCodes.BAD_REQUEST;
  }
}

