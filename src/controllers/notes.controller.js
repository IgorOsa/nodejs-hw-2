import {BadRequestError} from '../errors/errors';
import Note from '../models/notes.model';

const getAll = ({userId, offset, limit}) => {
  if (offset < 0) {
    throw new BadRequestError();
  }
  return Note.find({userId}, null, {skip: +offset, limit: +limit}).exec();
};

const getByid = async ({userId, id}) => {
  try {
    const note = await Note.findOne({userId, _id: id}).exec();

    if (!note) {
      throw new BadRequestError();
    }

    return note;
  } catch (err) {
    throw new BadRequestError();
  }
};

const create = async ({userId, text}) => {
  if (!userId || !text) {
    throw new BadRequestError();
  }

  const noteEntity = await Note.create({userId, text});

  if (!noteEntity) {
    throw new BadRequestError();
  }

  return noteEntity;
};

const update = async ({userId, id, ...rest}) => {
  if (!userId || !id) {
    throw new BadRequestError();
  }

  const updated = await Note.findOneAndUpdate({_id: id, userId}, {...rest});

  if (!updated) {
    throw new BadRequestError();
  }

  return updated;
};

const remove = async ({userId, id}) => {
  try {
    const deletedNote = await Note.deleteOne({_id: id, userId});
    if (deletedNote.deletedCount !== 1) {
      throw new BadRequestError();
    }
    return deletedNote;
  } catch (err) {
    throw new BadRequestError();
  }
};

const removeMany = async (userId) => {
  try {
    const deleted = await Note.deleteMany({userId});

    if (deleted.deletedCount < 1) {
      throw new BadRequestError();
    }
    return deleted;
  } catch (err) {
    throw new BadRequestError();
  }
};

export default {getAll, getByid, create, update, remove, removeMany};
