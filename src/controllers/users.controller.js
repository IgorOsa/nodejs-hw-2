import {checkHashedPassword} from '../auth/hashHelpers';
import {BadRequestError} from '../errors/errors';
import Credentials from '../models/credentials.model';
import User from './../models/user.model';

const get = async (username) => {
  try {
    const user = await User.findOne({username}).exec();
    if (!user || user === null) {
      throw new BadRequestError();
    }
    return user;
  } catch (err) {
    throw new BadRequestError();
  }
};

const update = async (user) => {
  const {username, password, oldPassword} = user;

  const userEntity = await Credentials.findOne({username}).exec();

  if (!userEntity) {
    throw new BadRequestError();
  }

  const comparisonRes = await checkHashedPassword(oldPassword, userEntity.password);

  if (comparisonRes) {
    const updatedUser = await Credentials.findOneAndUpdate({username}, {$set: {username, password}}, {new: true});

    if (!updatedUser) {
      throw new BadRequestError();
    }

    return updatedUser;
  }
  throw new BadRequestError();
};

const remove = async (username) => {
  const deletedUser = await User.deleteOne({username});
  const deletedCredentials = await Credentials.deleteOne({username});
  if (deletedUser.deletedCount !== 1 || deletedCredentials.deletedCount !== 1) {
    throw new BadRequestError();
  }
  return deletedUser;
};

export default {get, update, remove};
