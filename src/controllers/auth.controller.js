import jwt from 'jsonwebtoken';
import User from './../models/user.model';
import Credentials from '../models/credentials.model';
import {BadRequestError} from '../errors/errors';
import {checkHashedPassword} from '../auth/hashHelpers';
import {JWT_SECRET_KEY, JWT_EXPIRE_TIME} from '../common/config';

const create = async (user) => {
  try {
    const {username, password} = user;
    if (!username || !password) {
      throw new BadRequestError();
    }
    await User.create({username});
    await Credentials.create({username, password});
  } catch (err) {
    throw new BadRequestError();
  }
};

const login = async ({username, password}) => {
  try {
    if (!username || !password) {
      throw new BadRequestError();
    }

    const userEntity = await Credentials.findOne({username}).exec();

    if (!userEntity) {
      throw new BadRequestError();
    }

    const comparisonRes = await checkHashedPassword(password, userEntity.password);

    if (comparisonRes) {
      const {_id} = userEntity;
      const token = jwt.sign({userId: _id, username}, JWT_SECRET_KEY, {
        expiresIn: JWT_EXPIRE_TIME
      });
      return token;
    }

    throw new BadRequestError();
  } catch (err) {
    throw new BadRequestError();
  }
};

export default {create, login};
