import * as bcrypt from 'bcrypt';
import {DEFAULT_SALT_ROUNDS} from '../common/config';

export const hashPassword = async (password) => {
  const salt = await bcrypt.genSalt(DEFAULT_SALT_ROUNDS);
  const hash = await bcrypt.hash(password, salt);

  return hash;
};

export const checkHashedPassword = async (password, hash) => bcrypt.compare(password, hash);
