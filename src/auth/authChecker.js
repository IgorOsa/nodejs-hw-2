import jwt from 'jsonwebtoken';
import {JWT_SECRET_KEY} from '../common/config';
import {BadRequestError} from '../errors/errors';
import asyncWrapper from './../common/asyncWrapper';

export default asyncWrapper(async (req, res, next) => {
  const authHeader = req.header('Authorization');
  res.locals.auth = '';

  if (authHeader !== undefined) {
    const [token] = authHeader
        .match(/[A-Za-z0-9-_=]+\.[A-Za-z0-9-_=]+\.?[A-Za-z0-9-_.+/=]*/);

    if (!token) {
      throw new BadRequestError();
    }

    try {
      await jwt.verify(token, JWT_SECRET_KEY);
    } catch (err) {
      throw new BadRequestError();
    }

    res.locals.auth = await jwt.decode(token);

    return next();
  }

  throw new BadRequestError();
});
