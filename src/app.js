import express from 'express';
import swaggerUI from 'swagger-ui-express';
import authChecker from './auth/authChecker';
import {swaggerDocument} from './common/config';
import {errorHandler} from './errors/errorHandler';
import logger from './common/logger';
import authRouter from './routes/auth.router';
import usersRouter from './routes/users.router';
import notesRouter from './routes/notes.router';

const app = express();

app.use(express.json());

app.use('/doc', swaggerUI.serve, swaggerUI.setup(swaggerDocument));

const greeting = {message: 'Welcome to HW#2 server!'};

app.use(logger.logToConsole);

app.get('/api', (req, res, next) => {
  if (req.originalUrl === '/api') {
    res.send(greeting);
    return;
  }
  next();
});

app.use('/api/auth', authRouter);
app.use('/api/users', authChecker, usersRouter);
app.use('/api/notes', authChecker, notesRouter);

app.use(errorHandler);

export default app;
