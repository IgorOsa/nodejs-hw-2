import express from 'express';
import asyncWrapper from './../common/asyncWrapper';
import authController from './../controllers/auth.controller';

const router = new express.Router();

router.route('/register').post(
    asyncWrapper(async (req, res) => {
      await authController.create(req.body);
      res.json({message: 'Success'});
    })
);

router.route('/login').post(
    asyncWrapper(async (req, res) => {
      const token = await authController.login(req.body);
      res.json({message: 'Success', jwt_token: token});
    })
);

export default router;
