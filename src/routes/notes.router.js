import express from 'express';
import asyncWrapper from '../common/asyncWrapper';
import notesController from '../controllers/notes.controller';
import {BadRequestError} from '../errors/errors';

const router = new express.Router();

router.route('/')
    .get(
        asyncWrapper(async (req, res) => {
          const {userId} = res.locals.auth;
          const offset = req?.query?.offset || 0;
          const limit = req?.query?.limit || 0;
          const notes = await notesController.getAll({userId, offset, limit});
          res.json({offset: +offset, limit: +limit, count: notes.length, notes});
        })
    )
    .post(
        asyncWrapper(async (req, res) => {
          const {userId} = res.locals.auth;
          const {text} = req.body;
          await notesController.create({userId, text});
          res.json({'message': 'Success'});
        })
    );

router.route('/:id')
    .get(
        asyncWrapper(async (req, res) => {
          const {userId} = res.locals.auth;
          const {id} = req.params;
          const note = await notesController.getByid({userId, id});
          res.json({note});
        })
    )
    .put(
        asyncWrapper(async (req, res) => {
          const {userId} = res.locals.auth;
          const {id} = req.params;
          const {text} = req.body;
          if (!text) {
            throw new BadRequestError();
          }
          await notesController.update({userId, id, text});
          res.json({message: 'Success'});
        })
    )
    .patch(
        asyncWrapper(async (req, res) => {
          const {userId} = res.locals.auth;
          const {id} = req.params;
          const note = await notesController.getByid({userId, id});
          const {completed} = note;
          await notesController.update({userId, id, completed: !completed});
          res.json({message: 'Success'});
        })
    )
    .delete(
        asyncWrapper(async (req, res) => {
          const {userId} = res.locals.auth;
          const {id} = req.params;
          await notesController.remove({id, userId});
          res.json({message: 'Success'});
        })
    );

export default router;
