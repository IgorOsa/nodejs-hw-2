import express from 'express';
import notesController from '../controllers/notes.controller';
import {BadRequestError} from '../errors/errors';
import asyncWrapper from './../common/asyncWrapper';
import usersController from './../controllers/users.controller';

const router = new express.Router();

router.route('/me')
    .get(
        asyncWrapper(async (req, res) => {
          const {username} = res.locals.auth;
          const user = await usersController.get(username);
          res.json({user});
        })
    )
    .patch(
        asyncWrapper(async (req, res) => {
          const {username} = res.locals.auth;
          const {oldPassword, newPassword} = req.body;

          if (!username || !oldPassword || !newPassword) {
            throw new BadRequestError();
          }
          await usersController.update({username, password: newPassword, oldPassword});
          res.json({message: 'Success'});
        })
    )
    .delete(
        asyncWrapper(async (req, res) => {
          const {username, userId} = res.locals.auth;
          if (!username || !userId) {
            throw new BadRequestError();
          }
          await notesController.removeMany(userId);
          await usersController.remove(username);
          res.json({message: 'Success'});
        })
    );

export default router;
