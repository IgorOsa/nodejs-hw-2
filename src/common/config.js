import dotenv from 'dotenv';
import path, {dirname} from 'path';
import {fileURLToPath} from 'url';
import YAML from 'yamljs';

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

export const swaggerDocument = YAML.load(path.join(__dirname, '../../doc/openapi.yaml'));

dotenv.config({
  path: path.join(__dirname, '../../.env')
});

export const NODE_ENV = process.env.NODE_ENV;
export const PORT = process.env.PORT;
export const MONGO_CONNECTION_STRING = process.env.MONGO_CONNECTION_STRING;
export const DEFAULT_SALT_ROUNDS = parseInt(
    process.env.DEFAULT_SALT_ROUNDS,
    10
);
export const JWT_SECRET_KEY = process.env.JWT_SECRET_KEY;
export const JWT_EXPIRE_TIME = process.env.JWT_EXPIRE_TIME;

export default {PORT, NODE_ENV};
