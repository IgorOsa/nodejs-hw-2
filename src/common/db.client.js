import mongoose from 'mongoose';
import {MONGO_CONNECTION_STRING} from './config';

const connectToDB = (cb) => {
  mongoose.connect(MONGO_CONNECTION_STRING, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  });

  const db = mongoose.connection;
  db.on('error', console.error.bind(console, 'connection error:'));
  db.once('open', () => {
    console.log('Connected to MongoDB!');
    cb();
  });
};

export default connectToDB;
