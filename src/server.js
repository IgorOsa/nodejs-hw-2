import {PORT} from './common/config';
import app from './app';
import connectToDB from './common/db.client';

connectToDB(() => {
  const server = app.listen(PORT, () => {
    console.log(`App is running on http://localhost:${PORT}`);
  });
  server.on('error', console.error);
});
